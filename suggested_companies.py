#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  5 11:31:35 2018

@author: HuanLiang
"""




import pandas as pd

teams = pd.read_csv('/Users/HuanLiang/Desktop/SU/production_data/teams.csv')
item_platform_subplatform = pd.read_csv('/Users/HuanLiang/Desktop/SU/production_data/item_platform_subplatform.csv')
item_values_audits_brands = pd.read_csv('/Users/HuanLiang/Desktop/SU/production_data/item_values_audits_all_leagues.csv')

#simply just reading the csv files

all_items = item_platform_subplatform[~item_platform_subplatform['item_name'].str.contains("\\((Basketball|Baseball|Hockey|Football|Soccer)\\)")]

# making the item name have
for team_index, team_row in teams.iterrows():
    #print("... working on team: " + team_row.team_name)
    if team_row.league_id == 1:
        item_team = item_platform_subplatform[item_platform_subplatform['item_name'].str.contains("\\((Football)\\)")].append(all_items)
    elif team_row.league_id == 2:
        item_team = item_platform_subplatform[item_platform_subplatform['item_name'].str.contains("\\((Basketball)\\)")].append(all_items)
    elif team_row.league_id == 3:
        item_team = item_platform_subplatform[item_platform_subplatform['item_name'].str.contains("\\((Baseball)\\)")].append(all_items)
    elif team_row.league_id == 4:
        item_team = item_platform_subplatform[item_platform_subplatform['item_name'].str.contains("\\((Soccer)\\)")].append(all_items)
    elif team_row.league_id == 5:
        item_team = item_platform_subplatform[item_platform_subplatform['item_name'].str.contains("\\((Hockey)\\)")].append(all_items)

    item_team.insert(0, 'team_id', team_row.team_id)
    item_team.insert(1, 'team_name', team_row.team_name)
    item_team.insert(2, 'league_id', team_row.league_id)



    item_team.insert(item_team.shape[1], 'total_brands', 0)



    item_team.insert(item_team.shape[1], 'total_unique_brands', 0)
    item_team.insert(item_team.shape[1], 'total_unique_categories', 0)
    item_team.insert(item_team.shape[1], 'total_unique_subcategories', 0)
    item_team.insert(item_team.shape[1], 'brand_names', "")

    team_name = team_row['team_name'].replace(' ', '-').lower()

    for index, row in item_team.iterrows():
        team_audits = item_values_audits_brands.query("item_id==" + str(row['item_id']) + " & team_id==" + str(row['team_id']))

        brands = team_audits[['brand_id', 'brand_name']].groupby(['brand_name']).count()

        brand_list = ""
        for brand_index, brand_row in brands.iterrows():
            brand_list = brand_list + brand_index + "; "
        item_team.loc[[index],['brand_names']] = brand_list;

        item_team.loc[[index],['total_brands']] = brands['brand_id'].sum()
        item_team.loc[[index],['total_unique_brands']] = brands.size

        categories = team_audits[['category_id', 'category_name']].groupby(['category_name']).count()
        item_team.loc[[index],['total_unique_categories']] = categories.size

        subcategories = team_audits[['subcategory_id', 'subcategory_name']].groupby(['subcategory_name']).count()
        item_team.loc[[index],['total_unique_subcategories']] = subcategories.size

        #item_team.to_csv('/Users/HuanLiang/Desktop/SU/output/' + str(team_row['league_id']) + "-" + team_name + '.csv', index=False)'''
