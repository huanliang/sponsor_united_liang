#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 21 17:07:01 2018

@author: HuanLiang
"""

import scrapy
import urllib.request


class LogosSpider(scrapy.Spider):
    name = "logos"
    
    DEPTH_PRIORITY = 1
    SCHEDULER_DISK_QUEUE = 'scrapy.squeues.PickleFifoDiskQueue'
    SCHEDULER_MEMORY_QUEUE = 'scrapy.squeues.FifoMemoryQueue'
    CONCURRENT_REQUESTS = 1

    def start_requests(self):
        with open('twit.txt') as f:
            urls = [line.rstrip('\n\r') for line in f]
        
        
        
            
        yield scrapy.Request(url=urls[0], callback=self.parse, meta={'urls': urls, 'current_index':0})
        
        
    def parse(self, response):
        
        priority_marker = 0
        for i in response.meta['urls']:
            
            current_index = response.meta['current_index'] + 1
            if current_index < len(response.meta['urls']):
                yield scrapy.Request(url=response.meta['urls'][current_index], callback=self.parse,
                                     meta={'urls': response.meta['urls'], 'current_index': current_index}, priority=priority_marker)
            priority_marker += 1
            
            

    
        logosrc = response.css('a.ProfileAvatar-container img::attr(src)').extract_first()

        webContent = ""
        fname = ""
        
        if logosrc:
            webfile = urllib.request.urlopen(logosrc)
            webContent = webfile.read()

        if webContent:
            fname = logosrc[logosrc.rfind("/")+1:]
            f = open('logos/' + fname, 'wb')
            f.write(webContent)
            f.close
    
        f1 = open('logosout.csv', 'a')
        
        f1.write(response.url + "," + fname)
        f1.write('\n')
        f1.close
