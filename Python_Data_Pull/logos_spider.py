#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar 21 17:07:01 2018

@author: HuanLiang
"""

import scrapy
import urllib.request
import os


class LogosSpider(scrapy.Spider):
    name = "logos"
    
    #Change depth-first to breadth-first, not sure is anything is changed
    DEPTH_PRIORITY = 1
    SCHEDULER_DISK_QUEUE = 'scrapy.squeues.PickleFifoDiskQueue'
    SCHEDULER_MEMORY_QUEUE = 'scrapy.squeues.FifoMemoryQueue'
    
    #Take one request at a time
    CONCURRENT_REQUESTS = 1
    
    
    def start_requests(self):
        
        #Open the Twitter urls 
        with open('twit.txt') as f:
            #Put them in a list
            urls = [line.rstrip('\n\r') for line in f]
    
        #Request to scrape the first url, give an index to the request   
        yield scrapy.Request(url=urls[0], callback=self.parse, meta={'urls': urls, 'current_index':0})
        
        
    def parse(self, response):
        
        #Add the current index by one
        current_index = response.meta['current_index'] + 1
        
        #yield the request with changing index and priority, as long as the index doesn't go beyond items in the list
        if current_index < len(response.meta['urls']):
            
            
            yield scrapy.Request(url=response.meta['urls'][current_index], callback=self.parse,
                                     meta={'urls': response.meta['urls'], 'current_index': current_index})
            
            
            

        #Below is just Alain's code..
        
        #logosrc is the logo on the twitter website
        logosrc = response.css('a.ProfileAvatar-container img::attr(src)').extract_first()
        webContent = ""
        fname = ""
        
        #if there is a logo to extract
        if logosrc:
            #The following two lines extract it.
            webfile = urllib.request.urlopen(logosrc)
            webContent = webfile.read()
        #if it can be extracted
        if webContent:
            #put it in logos folder
            fname = logosrc[logosrc.rfind("/")+1:]
            
            f = open('logos/' + fname, 'wb')
            
            f.write(webContent)
            
            print(response.url)
            
            f.close
        
        #I tried to rename all the logos with their company, keep if useful
        '''path = "/Users/HuanLiang/Desktop/SU/sponsor_united_liang/Python_Data_Pull/logos/"
        path_list = os.listdir(path)
        print(path_list[1:])
        for i in path_list[1:]: 
            
            
            os.renames(path+i, path+response.url+".jpeg")'''
        
        
        
        #make a csv file
        f1 = open('logosout.csv', 'a')
        
        f1.write(str(current_index) + ","+response.url + "," + fname)
        f1.write('\n')
        f1.close
